# Crono HDL CI

Useful tools to develop HDL code created originally for Crono TDC Project.

## Scripts

Useful scripts to setup and configure CI environment. For now, we only have a script that can be used to setup a gitlab runner.

To use this script you must have installed docker in the linux host, then execute the following command:

```
./setup_gitlab_runner.sh <REPO_OR_GROUP_REGISTER_GITLAB_TOKEN>
```

Where `<REPO_OR_GROUP_REGISTER_GITLAB_TOKEN>` it is the runner register token of the gitlab group or repository.

## Runner config
The runner config can be accessed from the linux host filesystem in `/srv/gitlab-runner/config/config.toml`.


## Troubleshoot

### Cache volume permissions
If an error of this kind appears:

```
ERROR: Job failed: adding cache volume: set volume permissions: running permission container "bcb6aaec5d24a75608c2ac695e24f431ef5016424c85ae267272132c61742560" for volume "runner-avxfwfnp-project-43205027-concurrent-0-cache-3c3f060a0374fc8bc39395164f415a70": waiting for permission container to finish: exit code 159
```

It is probably that the docker installation is bad. Just reinstall it.

### Prepare environment error

One time this error appears:

```
ERROR: Job failed (system failure): prepare environment: Error response from daemon: Cannot link to a non running container: /runner-tmnx4mz1-project-43205027-concurrent-0-a6eed7b109457b75-docker-0 AS /runner-tmnx4mz1-project-43205027-concurrent-0-a6eed7b109457b75-predefined-0/docker (exec.go:78:0s). Check https://docs.gitlab.com/runner/shells/index.html#shell-profile-loading for more information
```

We just rebooted the raspberry and everything works since then. Was weird, I think we reinstalled docker before this.

