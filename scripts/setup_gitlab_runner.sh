#!/bin/bash

###############################################################################
## Description:
## Script used to setup a gitlab runner to run the HDL pipelines for the Crono
## TDC development. 
## This script needs as a parameter the gitlab token to register the runner.
## Author: Julian Nicolas Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>

###############################################################################
## Variables
GITLAB_INSTANCE_URL=https://gitlab.com
GITLAB_RUNNER_DESCRIPTION="Laboratory Gitlab Runner!"
GITLAB_RUNNER_TAGS=docker-amd64 # default value
GITLAB_RUNNER_CONFIG_DIR=/srv/gitlab-runner/config/

##############################################################################
## Help message
HELP_MSG="
___________                            __    _________ .___    _________       __                
\_   _____/__________   ____   _______/  |_  \_   ___ \|   |  /   _____/ _____/  |_ __ ________  
 |    __)/  _ \_  __ \_/ __ \ /  ___/\   __\ /    \  \/|   |  \_____  \_/ __ \   __\  |  \____ \ 
 |     \(  <_> )  | \/\  ___/ \___ \  |  |   \     \___|   |  /        \  ___/|  | |  |  /  |_> >
 \___  / \____/|__|    \___  >____  > |__|   \______  /___| /_______  /\___  > __| |____/|   __/ 
     \/                    \/     \/                \/              \/     \/            |__|  

setup_gitlab_runner.sh -r <GITLAB_RUNNER_TOKEN> [-t <GITLAB_RUNNER_TAG>] [-p <GITLAB_RUNNER_DOCKER_IMAGE_PLATFORM>]

Script used to setup gitlab runner for Gitlab CI pipelines.
"

###############################################################################
## Functions
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
NC='\033[0m'

log_info() {
	        echo -e ${GREEN}"$(date +"%Y-%m-%dT%H:%M:%S.%03N") - $*"${NC}
	}

log_error() {
	        echo -e ${RED}"$(date +"%Y-%m-%dT%H:%M:%S.%03N") - $*"${NC}
	}

log_warning() {
	        echo -e ${ORANGE}"$(date +"%Y-%m-%dT%H:%M:%S.%03N") - $*"${NC}
	}

###############################################################################
## Parse arguments

while getopts ":r:t:c:h" opt; do
	case ${opt} in
		h)
			echo -e "${HELP_MSG}"
			exit 0
			;;

		r)
			GITLAB_RUNNER_REG_TOKEN=${OPTARG}
			echo "Registration token: ${GITLAB_RUNNER_REG_TOKEN}"
			;;
		t)
			GITLAB_RUNNER_TAGS=${OPTARG}
			echo "Gitlab Runner tag: ${GITLAB_RUNNER_TAGS}"
			;;	
		c)
			GITLAB_RUNNER_CONFIG_DIR=${OPTARG}
			echo "Gitlab Runner config dir: ${GITLAB_RUNNER_CONFIG_DIR}"
			;;

		\?)
			echo "Invalid option: -$OPTARG"
			exit 1
			;;
		:)
			echo "The option -$OPTARG requires an argument."
			exit 1
			;;
	esac
done

if [ -z "${GITLAB_RUNNER_REG_TOKEN}" ]; then
	log_error "Missing registration token! (-r)"
	exit -1
fi

log_info "Gitlab Runner tags: ${GITLAB_RUNNER_TAGS}"
log_info "Gitlab Runner docker image: gitlab/gitlab-runner"

###############################################################################
## Setup start
if [ -n "$(podman --version 2> /dev/null)" ]
then
    log_info "podman is installed in this PC!"
    DOCKER_EXTRA_ARGS+="--group-add keep-groups"
fi

if [ "$( docker container inspect -f '{{.State.Running}}' gitlab-runner )" != "true" ]; then
    log_info "Gitlab runner container does not exists! creating it..."
	mkdir -p ${GITLAB_RUNNER_CONFIG_DIR}
	docker run -d --name gitlab-runner --restart always --net=host \
		-v ${GITLAB_RUNNER_CONFIG_DIR}:/etc/gitlab-runner \
		-v /var/run/docker.sock:/var/run/docker.sock \
		gitlab/gitlab-runner:latest
fi

log_info "registering runner with docker executor using gitlab-runner docker image"
docker exec gitlab-runner \
    gitlab-runner register -n \
    --url ${GITLAB_INSTANCE_URL} \
    --registration-token ${GITLAB_RUNNER_REG_TOKEN} \
    --tag-list ${GITLAB_RUNNER_TAGS} \
    --docker-image "docker:20.10.16" \
    --executor docker \
    --output-limit 10000 \
    --description ${GITLAB_RUNNER_DESCRIPTION} \
    --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
    --docker-volumes "/certs/client" \
    --docker-volumes /cache \
    --docker-network-mode "host" \

log_info "checking if gitlab-runner is active (will delete unregistered runners)"
docker exec gitlab-runner gitlab-runner verify --delete
